"use strict";
const container = document.querySelector('.posts');
const API = "https://ajax.test-danit.com/api/json/";
let cardArray = [];
const titleInput = document.querySelector('.title-input');
const textInput = document.querySelector('.text-input');
const modal = document.querySelector('.add_modal');

const sendRequest = async (url, method = 'GET', config = null) => {
  const request = await fetch(url, {
    method: method,
    ...config
  });
  let result = await request;
  if (method === "GET") {
    return result.json();
  } else {
    return result;
  }
}

const getUsers = () => {
  sendRequest(`${API}users`)
    .then(data => {
      data.forEach(({ id, name, email }) => {
        let card = new Card(id, name, email);
        card.createUserCard();
        card.getPost();
        cardArray.push(card);
      });
    })
    .catch((error) => {
      console.log(error.message);
    })
}

class Card {
  constructor(id, name, email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }

  getPost() {
    sendRequest(`${API}posts/${this.id}`)
      .then(post => {
        this.cardElement.lastElementChild.remove();
        this.createUserPost(post)
      })
      .catch((error) => {
        console.log(error.message);
      })

  }

  sendPost(title, body) {
    this.createUserCard();
    const postBody = { title, body };
    sendRequest(`${API}posts/`, 'POST', {
      body: JSON.stringify(postBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(() => {
        this.cardElement.lastElementChild.remove();
        this.createUserPost(postBody);
      })
      .catch((error) => {
        console.log(error.message);
      })

  }

  sendChangedPost(newContent) {
    this.cardElement.querySelector('.change').remove();
    this.cardElement.insertAdjacentHTML('beforeend', `
    <div class="loader"></div>
    `);
    sendRequest(`${API}posts/${this.id}`, 'PUT', {
      body: JSON.stringify(newContent),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(() => {
        this.cardElement.lastElementChild.remove();
        this.createUserPost(newContent);
      })
      .catch((error) => {
        console.log(error.message);
      })
  }
 
  createUserPost({ title, body }) {
    this.cardElement.insertAdjacentHTML('beforeend', `
  <div class="post__content">
    <div class="post__title">${title}</div>
    <div class="post__text">${body}</div>
  </div>
  `)
  };

  createUserCard() {
    container.insertAdjacentHTML('afterbegin', `
    <div class="post" id="${this.id}">
        <div class="post__header">
            <div class="post__user">
                <h3 class="post__user-name">${this.name}</h3>
                <a href="mailto:${this.email}" class="post__user-email">${this.email}</a>
                </div>
                <div class="buttons">
                <button class="btn-close">x</button>
                <button class="btn-change"> . . . </button>
            </div>
        </div>
        <div class="loader"></div>
    </div>
  `)
    this.cardElement = container.querySelector(`.post[id="${this.id}"]`);
  }

  editForm(){
    this.cardElement.lastElementChild.remove();
    this.cardElement.insertAdjacentHTML('beforeend', `
    <div class="post__content change">
      <input class="post__title-input" type="text">
      <textarea class="post__text-input" type="text"></textarea>
    </div>
    `)
  }

  deleteUser() {
    sendRequest(`${API}users/${this.id}`, 'DELETE')
      .then(() => {
        this.removeCardElement();
      })
      .catch((error) => {
        console.log(error.message);
      })
  }

  removeCardElement() {
    this.cardElement.remove();
  }
}

document.querySelector('.add').addEventListener('click', (e) => {
  const target = e.target;
  if (target.classList.contains('add-btn')) {
    modal.classList.toggle('show');
  }
  if (target.classList.contains('send-btn')) {
    if (titleInput.value && textInput.value) {
      let userCard = new Card(0, 'User', 'User@gmail.com');
      userCard.sendPost(titleInput.value, textInput.value);
      cardArray.push(userCard);
      textInput.value = "";
      titleInput.value = "";
      modal.classList.remove('show');
    } else {
      alert("Відсутній текст!");
    }
  }
});

document.querySelector('.posts').addEventListener('click', (e) => {
  const target = e.target;
  const idElement = +target.closest('.post').id;
  const targetCard = cardArray.find(card => card.id === idElement);
  if (target.classList.contains('btn-close')) {
    targetCard.deleteUser();
  }
  if (target.classList.contains('btn-change')) {
   target.classList.toggle('inchange');
   if (target.classList.contains('inchange')) {
    target.innerText = "Save";
    targetCard.editForm();
   } else {
     const title = document.querySelector('.post__title-input').value;
     const body = document.querySelector('.post__text-input').value;
     target.innerText = "Change";
     if (title && body) {
       const newContent = {title,body};
       targetCard.sendChangedPost(newContent);
     } else {
      alert("Відсутня інформація!");
      target.classList.toggle('inchange');
      target.innerText = "Зберегти";
     }
   }
  }
});

getUsers(API);



