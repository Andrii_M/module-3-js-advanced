class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this.age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        return this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        return this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        return this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary);
        this._lang = lang;
    }
    get lang() {
        return this._lang;
    }

    set lang(newlang) {
        return this._lang = newlang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const prog1 = new Programmer ("Andrew", '29', '29000',["eng", "ua", "ru"]);
const prog2 = new Programmer ("Michel", '18', '18000',["fr", "ua", "pl", "es"]);
const prog3 = new Programmer ("Kirill", '23', '25000',["eng", "ar", "fr"]);
const prog4 = new Programmer ("Oleksandr", '31', '35000',["fr", "es", "pl"]);
const prog5 = new Programmer ("Alona", '37', '125000',["fr", "es", "pl", "eng", "ar","ua", "ru"]);

console.log(prog1);
console.log(prog2);
console.log(prog3);
console.log(prog4);
console.log(prog5);
console.log(prog1.salary);
console.log(prog3.salary);
console.log(prog5.salary);
