const API = "https://api.ipify.org/?format=json";
const infoAPI = "http://ip-api.com/json/";
const filter = "?fields=continent,country,regionName,city,district";
const btn = document.querySelector('.btn');
const wrapper = document.querySelector('.wrapper');

const sendRequest = (url) => fetch(url).then(response => response.json());

btn.addEventListener('click', async()=>{
  wrapper.innerText = "";
  wrapper.insertAdjacentHTML('afterbegin',`
  <div class="info">
  <h2 class="info__title">Отримана інформація</h2>
  <div class="loader"></div>
  </div>
  `);
  const infoCard = wrapper.querySelector('.info');
  try {
    const ipObj = await sendRequest(API);
    const data = await sendRequest(`${infoAPI}${ipObj.ip}${filter}`);
    infoCard.lastElementChild.remove();
    const {continent,country,regionName,city,district} = data;
    infoCard.insertAdjacentHTML('beforeend',`
          <ul class = "info__ip">
            <li>Континент</li>
            <li>Країна</li>
            <li>Pегіон</li>
            <li>Місто</li>
          </ul>
          <ul class = "info__ip">
            <li>${continent}</li>
            <li>${country}</li>
            <li>${regionName}</li>
            <li>${city}</li>
          </ul>
    `);
  } catch (error) {
    infoCard.lastElementChild.remove();
    console.log(error);
  }
})