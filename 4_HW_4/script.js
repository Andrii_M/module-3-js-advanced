const API = "https://ajax.test-danit.com/api/swapi/films";

const sendRequest = async (url) => {
	const response = await fetch(url);
	const result = await response.json();
	return result;
}

const container = document.querySelector('#movies');

const getCharacters = (data) => {
  const movie = container.querySelectorAll('.film');
  movie.forEach((film) => {
    const movieInfo = data.find((info) => info.id == film.id);
    const requests = movieInfo.characters.map((url) => fetch(url));
    Promise.all(requests)
    .then((responses) => {
      const results = responses.map((response) => response.json());
      Promise.all(results)
        .then((results) => {
          let nameList = results.map((person) => `<li>${person.name}</li>`);
          renderCharacters(nameList, film);
        })
        .catch((error) => {
          console.log(error.message);
        })
        .finally(() => {
          const loading = film.querySelector(".loader");
          loading.remove();
        });
    });
  });
};


const getMovie = (url) => {
  sendRequest(url)
    .then(data => {
      data.forEach(film => {
        createPostMovie(film);
      });
        getCharacters(data);
    })
    .catch((error) => {
      console.log(error.message);
    })
}


const createPostMovie = ({ episodeId, name, openingCrawl }) => {
  container.insertAdjacentHTML('afterbegin', `
        <div class="film" id="${episodeId}">
            <h2 class="film__name">${name}</h2>
            <div class="film__plot">
                <p class="film__text">${openingCrawl}</p>
            </div>
            <div class="loader"></div>
        </div>
  `)
}

const renderCharacters = (arr, parentElement) => {
  parentElement.insertAdjacentHTML('beforeend', `
        <div class = "film__characters">
            <h2>Characters</h2>
            <ul>
                ${arr.join(" ")}
            </ul>
        </div>
  `)
}

getMovie(API);


